import os
import db
import paths

import log

def list_dir(subpath):
    if subpath.startswith('Transaction'):
        subpath = subpath.replace('Transaction','transacoes')
    path = paths.path + '/' + subpath
    norm_path = os.path.normpath(path)
    if not norm_path.startswith(paths.path):
        return []
    return sorted(list(os.listdir(path)),reverse=True)


def find_smaller_bigger(vector,value,small,big):
    if vector[small] == value:
        return small, small
    if vector[big] == value:
        return big, big
    if big -small  == 1:
        return small, big
    mid = int((small + big) / 2)
    if value > vector[mid]:
        return find_smaller_bigger(vector,value,mid,big)
    if value < vector[mid]:
        return find_smaller_bigger(vector,value,small,mid)
    return mid, mid

def find_interval(start,end,max_len):
    open_door = db.Table(paths.jsons+'/OpenDoor.json').get('open',[])
    last = len(open_door) -1
    _,small = find_smaller_bigger(open_door,start,0,last)
    big,_ = find_smaller_bigger(open_door,end,0,last)
    if not max_len is None and big - small > max_len:
        big = max_len + small
    return open_door[small:big]



    
