from threading import Thread
from threading import Semaphore
from PIL import Image
from PIL import ImageFile
import numpy as np
import subprocess
import serial
import json 
import time
import math
import cv2
import os

import paths
import db
import log
import uget

ImageFile.LOAD_TRUNCATED_IMAGES = True

HDR_TIMES = np.array([20, 80, 160], dtype=np.float32)

sucessos = 0
cap_mutex = Semaphore()
goal = 250000
threshhold = 40000


cam_config = db.Table(paths.jsons+'/CamConfig.json')
exposure_points = db.Table(paths.jsons+'/Exposure.json')
invert = cam_config.get('invert',['',False,False,False,False])
ramps  = cam_config.get('ramps',['',10,10,10,10])
width = cam_config.get('width',640)
height = cam_config.get('height',480)
shelves = None

def setup():
    global shelves
    if uget.config['CAMERA_MODEL'] == 'USB':
        shelves = cam_config.get('shelves',['',0,2,4,6])
        v4l2_config('0',cam_config.get('0',{'exposure_auto':'3','exposure_absolute':'60'}))
        v4l2_config('2',cam_config.get('2',{'exposure_auto':'3','exposure_absolute':'60'}))
        v4l2_config('4',cam_config.get('4',{'exposure_auto':'3','exposure_absolute':'60'}))
        v4l2_config('6',cam_config.get('6',{'exposure_auto':'3','exposure_absolute':'60'}))
    else:
        shelves = cam_config.get('shelves',['',1,2,3,4])

def list_usb():
    full = subprocess.check_output(['lsusb']).decode("utf-8")
    lines = full.split('\n')
    summary = {}
    smaller = '002'
    for line in lines:
        if line == '':
            break
        words = line.split(' ')
        number =  words[3][0:3]
        name =  words[6]
        summary[name] = number
    if len(lines) > 2:
        smaller = lines[len(lines) - 3].split(' ')[3][0:3] 
    return summary, smaller

def check_hub():
    try:
        devices, smaller = list_usb()
        log.log('USb Devices: ' + str(devices))
        log.log('Smaller: ' + smaller)
        if len(devices) == 2 and ('Terminus' in devices):
            cap_mutex.acquire()
            log.log('Reseting hub...')
            time.sleep(2)
            output = subprocess.check_output(['usbreset', '001/' + devices['Terminus']]).decode('utf-8')
            time.sleep(5)
            log.log('Usb Devices: ' + str(list_usb()))
            cap_mutex.release()
        else: 
            cap_mutex.acquire()
            log.log('Reseting hub...')
            time.sleep(2)
            output = subprocess.check_output(['usbreset', '001/' + smaller]).decode('utf-8')
            time.sleep(5)
            log.log('Usb Devices: ' + str(list_usb()))
            cap_mutex.release()
    except Exception as e:
        log.log('Erro no check_hub ' + str(e))
        cap_mutex.release()

def check_reset():
    if uget.last_picture != 0 and uget.last_picture - time.time() > 1800 and uget.current_event is None:
        log.log('Too long without sucessful image, reseting..')
        os.system('reboot')
        exit()

class Capture(Thread):
    def __init__ (self,path,usb,moment):
        Thread.__init__(self)
        self.path = path
        self.moment = moment
        self.usb = usb
    def run(self):
        save_image(self.path,self.moment,self.usb)

class SupplyImages(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.running = True
    def stop(self):
        self.running =False
    def run(self):
        while(self.running):
            cap_mutex.acquire()
            capture_images('teste','teste')
            cap_mutex.release()
            time.sleep(1)


class Constant_Captures(Thread):
    def __init__ (self,path):
        Thread.__init__(self)
        self.path = path
    def run(self):
        log.log('Capturas Constantes')
        start_time = time.time()
        delay = 0
        while(delay < 60):
            capture_images('delay_'+str(delay)+'_',self.path)
            delay = math.floor(time.time() - start_time)
        

def save_image(path,moment,port):
    global sucessos
    log.log("USB é o "+port);
    try:
        usb = serial.Serial("/dev/ttyUSB"+port,1000000,timeout=1)
    except:
        log.log("USB"+port + " está vazio.")
        return
    usb.write(b'a')
    prat = ord(usb.read())
    if prat == 67:
        log.log("USB"+port+" camera init failed")
        return
    prat = shelves[prat]
    tam = int.from_bytes(usb.read(4),byteorder='little')
    log.log("USB"+port+" tamanho:  " + str(tam))
    log.log("USB"+port+" prateleira:  " + str(prat))
    image = usb.read(tam)
    if tam < 10000 or len(image) != tam:
        log.log('Suspeita de foto branca')
        return
    f = open(paths.transacoes+'/'+path+"/"+moment+str(prat)+'.jpg',"wb")
    f.write(image)
    f.close()
    usb.close()
    if not invert[prat]:
        invert_image(paths.transacoes+'/'+path+"/"+moment+str(prat)+'.jpg')
    sucessos += 1
    log.log('Imagem'+path+'/'+moment+str(prat)+'.jpg salva')

def brightness(frame):
    val = 0
    w,h,chanels = frame.shape
    for x in range(0,w,80):
        for y in range(0,h,20):
            val += int(frame[x][y][0]) + int(frame[x][y][1]) + int(frame[x][y][2])
    if width == 640:
        return 3*val
    return val

def fix_exposure(port,x1,y1,sen):
    try:
        x2 = x1 + 300*sen
        set_cam_config(str(port),{'exposure_absolute':str(x2)})
        cap = cv2.VideoCapture(int(port))
        ret,frame = cap.read()
        if ret:
            y2 = brightness(frame)
            new_exp = find_exposure_log(x1,y1,x2,y2)
            if new_exp == 0:
                new_exp = 3
            log.log('Nova exp: ' + str(new_exp))
            if new_exp > 2 and new_exp < 500:
                set_cam_config(str(port),{'exposure_absolute':str(new_exp)})
            else:
                set_cam_config(str(port),{'exposure_absolute':str(20)})
    except:
        set_cam_config(str(port),{'exposure_absolute':str(20)})

def find_exposure_log(x1,y1,x2,y2):
    a = (y2 - y1) / (math.log(x2/x1) + 0.01)
    b = math.exp(y1/a) / x1
    return int(math.exp(250000/a) / b)

def save_image_usb(path,moment,prat,slow=False):
    global width
    global height
    port = shelves[int(prat)]
    cap = cv2.VideoCapture(int(port))
    try:
        cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        i=0
        bri = 1000000
        frame = None
        while i < int(ramps[int(prat)]) and (bri > 300000 or bri < 200000):
            ret, frame = cap.read()
            bri = brightness(frame)
            i += 1
        if width == 1280:
            frame = cv2.resize(frame,(640,360),interpolation = cv2.INTER_AREA)
        if invert[int(prat)]:
            frame = cv2.rotate(frame,cv2.ROTATE_180)    
        cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+'.jpg',frame)
        cap.release()

        if slow:
            conf = get_cam_config_json(str(port))
            mode = int(conf['exposure_auto'])
            exp = int(conf['exposure_absolute'])
            day_time = time.time() % 86400
            if mode == 1:
                if bri > 300000:
                    fix_exposure(port,exp,bri,-1)
                elif bri < 200000:
                    fix_exposure(port,exp,bri,+1)
                elif bri > 260000:
                    set_cam_config(str(port),{'exposure_absolute':str(exp - 10)})
                elif bri < 240000:
                    set_cam_config(str(port),{'exposure_absolute':str(exp + 10)})
            log.log('Imagem'+path+'/'+moment+''+prat+'.jpg salva :' + str(bri))
            exposure_points.add('points',(prat,bri,mode,exp,day_time,moment,uget.config['MACHINE_ID']))

        return 1 if ret else 0
    except Exception as e:
        cap.release()
        log.log(str(e))
        return 0


def invert_image(path):
    log.log('invertendo imagem '+path)
    image = Image.open(path)
    image = image.rotate(180)
    image.save(path,"JPEG")

def capture_images(moment,path,slow=False):
    global sucessos
    global ramps
    global invert
    sucessos = 0
    try:
        log.log('Capturando fotos: '+moment)
        if uget.config['CAMERA_MODEL'] == 'USB':
            for i in range(1,5):
                try:
                    sucessos += save_image_usb(path,moment,str(i),slow)
                except:
                    pass
        else:
            threads = []
            for i in range(4):
                threads.append(Capture(path,str(i),moment))
                threads[i].start()
            for t in threads:
                t.join()
        log.log('Fotos capturadas: '+str(sucessos))
    except Exception as e:
        log.log('Erro de captura: '+str(e))
        cap_mutex.release()
    return sucessos

def get_cam_config(port):
    text = subprocess.check_output(['v4l2-ctl','-d','/dev/video' + str(port),'--list-ctrls']).decode()
    lines = text.split('\n')
    lines.pop()
    res = ''
    for line in lines:
        maximun = 1
        minimun = 0
        value = -1
        words = list(filter(lambda x: x != '',line.split(' ')))
        nome = words[0]
        words = list(filter(lambda x: '=' in x,words))
        for word in words :
            subwords = word.split('=')
            if subwords[0] == 'max':
                maximun = subwords[1]
            if subwords[0] == 'min':
                minimun = subwords[1]
            if subwords[0] == 'value':
                value = subwords[1]
        res += "<p> {} : <input type='number' min='{}' max='{}' value='{}' name='{}'></p>\n".format(nome,minimun,maximun,value,nome)
    return res

def get_cam_config_json(port):
    text = subprocess.check_output(['v4l2-ctl','-d','/dev/video' + str(port),'--list-ctrls']).decode()
    lines = text.split('\n')
    lines.pop()
    res = {}
    for line in lines:
        value = -1
        words = list(filter(lambda x: x != '',line.split(' ')))
        nome = words[0]
        words = list(filter(lambda x: '=' in x,words))
        for word in words :
            subwords = word.split('=')
            if subwords[0] == 'value':
                res[nome] = subwords[1]
    return res

def set_cam_config(port,form):
    global ramps
    global invert
    global width
    global height
    log.log(json.dumps(form))
    shelf = get_shelf(port)
    if 'shelf' in form:  
        if uget.config['CAMERA_MODEL'] == 'USB':
            swap_shelf_usb(port,form['shelf'])
        else:
            swap_shelf(port,form['shelf'])
        del form['shelf']
    if 'ramp' in form:
        ramps[shelf] = int(form['ramp'])
        cam_config.update('ramps',ramps)
        del form['ramp']
    if 'invert' in form:
        invert[shelf] = int(form['invert']) == 1
        cam_config.update('invert',invert)
        del form['invert']
    if 'dimension' in form:
        width = int(form['dimension'].split(':')[0])
        height = int(form['dimension'].split(':')[1])
        cam_config.update('width',width)
        cam_config.update('height',height)
        del form['dimension']
    if len(form) > 0:
        v4l2_config(port,form)
        cam_config.update(port,form)

def v4l2_config(port,data):
    for key in data.keys():
        subprocess.call(['v4l2-ctl','-d','/dev/video'+port,'-c',key+'='+data[key]])

def get_shelf(port):
    if uget.config['CAMERA_MODEL'] == 'USB': 
        return shelves.index(int(port))
    else:
        return shelves[int(port)]

def swap_shelf(cam,swap_shelf):
    global shelves
    swap_cam = int(shelves.index(int(swap_shelf)))
    if int(cam) != swap_cam:
        curr_shelf = shelves[int(cam)]
        shelves[int(cam)] = int(swap_shelf)
        shelves[int(swap_cam)] = curr_shelf
        cam_config.update('shelves',shelves)

def swap_shelf_usb(port,swap_shelf):
    global shelves
    curr_shelf = int(shelves.index(int(port)))
    if swap_shelf != curr_shelf:
        swap_port = shelves[int(swap_shelf)]
        shelves[int(swap_shelf)] = int(port)
        shelves[int(curr_shelf)] = swap_port
        cam_config.update('shelves',shelves)
        
def swap_shelves(curr_shelf,swap_shelf):
    global shelves
    port = shelves[int(curr_shelf)]
    if swap_shelf != curr_shelf:
        swap_port = shelves[int(swap_shelf)]
        shelves[int(swap_shelf)] = int(port)
        shelves[int(curr_shelf)] = swap_port
        log.log("shelves:" + json.dumps(shelves))
        cam_config.update('shelves',shelves)

def save_image_hdr(path,moment,prat):
    global width
    global height
    port = shelves[int(prat)]
    cap = cv2.VideoCapture(int(port))
    try:
        cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        imgs = []
        for exp  in HDR_TIMES:
            cap.set(cv2.CAP_PROP_EXPOSURE, exp)
            ret, frame = cap.read()
            if not ret:
                raise Exception('Camera error')
            if width == 1280:
                frame = cv2.resize(frame,(640,360),interpolation = cv2.INTER_AREA)
            imgs.append(frame)
        merge_mertens = cv2.createMergeMertens()
        mertens = merge_mertens.process(imgs)
        frame = np.clip(mertens*255, 0, 255).astype('uint8')
        if invert[int(prat)]:
            frame = cv2.rotate(frame,cv2.ROTATE_180)    
        cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+'.jpg',frame)
        cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+str(0)+'.jpg',imgs[0])
        cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+str(1)+'.jpg',imgs[1])
        cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+str(2)+'.jpg',imgs[2])
        cap.release()
        log.log('Imagem '+path+'/'+moment+''+prat+'.jpg salva')
        return 1 if ret else 0
    except Exception as e:
        cap.release()
        log.log(str(e))
        return 0
    
def get_mean(cap):
    if cap is None:
        return 0
    peq = cv2.resize(cap, (10, 10))
    L, A, B = cv2.split(cv2.cvtColor(peq, cv2.COLOR_BGR2LAB))
    L = L/np.max(L)
    return np.mean(L)

def mean_error(cap):
    mean = brightness(cap)
    return abs(mean-goal)

def find_exposure(cap,a,b,error_a,error_b):
    log.log(str(a)+"::"+str(b))
    c = (a+b)/2
    cap.set(cv2.CAP_PROP_EXPOSURE,int(c))
    ret, frame = cap.read()
    error_c = mean_error(frame)
    log.log(error_c)
    if error_c < threshhold or (b-a) < 1:
        return frame, error_c
    if error_a < error_b:
        return find_exposure(cap,a,c,error_a,error_c)
    else:
        return find_exposure(cap,c,b,error_c,error_b)

def save_image_exp(path,moment,prat):
    try:
        log.log("Tirando foto: "+str(prat))
        port = shelves[int(prat)]
        cap = cv2.VideoCapture(int(port))
        cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc(*'MJPG'))
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

        ret, frame = cap.read()
        error = mean_error(frame)

        log.log("Erro: "+str(error))

        if error < threshhold:
            frame = cv2.resize(frame,(640,360),interpolation = cv2.INTER_AREA)
            cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+'.jpg',frame)
            cap.release()
            return 1

        cap.set(cv2.CAP_PROP_EXPOSURE,2)
        ret, frame = cap.read()
        error_a = mean_error(frame)

        cap.set(cv2.CAP_PROP_EXPOSURE,700)
        ret, frame = cap.read()
        error_b = mean_error(frame)

        frame, erro_c = find_exposure(cap,2,700,error_a,error_b)
        frame = cv2.resize(frame,(640,360),interpolation = cv2.INTER_AREA)
        cap.release()
        if not frame is None and erro_c < 50000:
            cv2.imwrite(paths.transacoes+'/'+path+"/"+moment+prat+'.jpg',frame)
            cap.release()
            return 1
        return 0
    except Exception as e:
        log.log('Erro do exp: '+str(e))

