from datetime import datetime
import RPi.GPIO as GPIO
import time

import db
import paths

SENSOR = 24


def timestamp():
    return datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

open_door = db.Table(paths.jsons+'/OpenDoor.json')

if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(SENSOR, GPIO.IN)
    while True:
        if GPIO.input(SENSOR) == 0:
            open_door.add('open',timestamp())
            print('open')
        time.sleep(1)
