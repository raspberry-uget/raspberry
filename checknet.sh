echo "Começando o checknet"
while true
do
    sleep 10m 
    ping -c1 8.8.8.8 > /dev/null
    if [ $? != 0 ] 
    then
        /sbin/ifdown wlan0
        sleep 10s 
        /sbin/ifup wlan0    

        ping -c1 8.8.8.8 > /dev/null
        if [ $? != 0 ] 
        then
            date
            echo "rebootando"
            sudo reboot
            sleep 1m
        else
            date
            echo "voltou com ifdown ifup"
        fi
    
    else
        date
        echo "internet funcionando"
    fi    
done
