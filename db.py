import os
import json

import log

#Uma tabela de pares chave:valor para usar com persistência entre execuções, salvando em um arquivo json.
class Table:

    #Para iniicar especifica o caminho $path do arquivo json, onde será lido e escrito a tabela
    #Se o arquivo for inválido, rescreve por um vazio.
    def __init__(self,path):
        self.path = path
        try:
           self.all()
        except:
           log.log('Erro no db' + path)
           f = open(self.path,'w')
           f.write('{}')
           f.close()

    #Adiciona o valor $value para a chave $key na tabela.
    #Se a chave já existe atualiza o valor.
    #Se o valor for um dicionário só atualiza as chaves presentes, mantendo as outras como estavam.
    #Se o valor for None remove a chave da tabela
    def update(self,key,value):
        obj = json.load(open(self.path))
        if value == None:
            obj.pop(key, None)
        elif not key in obj or not isinstance(value,dict):
            obj[key] = value
        else:
            for sub_key in value.keys():
                obj[key][sub_key] = value[sub_key]
        f = open(self.path,'w') 
        f.write(json.dumps(obj))
        f.flush()
        os.fsync(f.fileno())
        f.close()

    def add(self,key,value):
        obj = json.load(open(self.path))
        if not key in obj:
            obj[key] = [value]
        elif type(obj[key]) is list:
            obj[key].append(value)
        f = open(self.path,'w') 
        f.write(json.dumps(obj))
        f.flush()
        os.fsync(f.fileno())
        f.close()
    
    #Retorna a tabela toda como uma dict.
    def all(self):
        f = open(self.path)
        obj = json.load(f)
        f.close()
        return obj

    #Retorna um campo específico da tabela, se não tiver esse campo retorna default
    def get(self,key,default):
        f = open(self.path)
        obj = json.load(f)
        f.close()
        if key in obj:
            return obj[key]
        else:
            return default
