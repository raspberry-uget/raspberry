import RPi.GPIO as GPIO
import requests
import time

import log
import uget

TRAVA = 9
SENSOR = 24

error = False

def setup():
    if uget.config['LOCK_MODEL'] == 'GPIO':
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(TRAVA, GPIO.OUT)
        GPIO.setup(SENSOR, GPIO.IN)

def command_door(command):
    try:
        acao = ['fechar','abrir']
        log.log("Enviando comando para "+acao[int(command)] + " porta")
        if uget.config['LOCK_MODEL'] == 'ESP':
            response = requests.get('http://192.168.43.70/update?relay=1&state='+command,timeout=10)
            log.log("Resposta da trava: " + str(response))
        elif uget.config['LOCK_MODEL'] == 'GPIO':
            GPIO.output(TRAVA,int(command))
        return True
    except:
        return False

def open():
    global error
    log.log('Abrindo porta')
    command_door('1')
    error = False

def close():
    log.log('Fechando porta')
    command_door('0')

def sensor_door(default):
    global error
    try:
        if uget.config['LOCK_MODEL'] == 'ESP':
            response = requests.get('http://192.168.43.70/door_sensor_state',timeout=3).json()
            log.log("Status da porta "+ str(response))
            if response['door_closed'] == 0:
                return 'OPEN'
            if response['door_closed'] == 1:
                return 'CLOSED'
        elif uget.config['LOCK_MODEL'] == 'GPIO':
            res = [0,0]
            for i in range(10):
                res[GPIO.input(SENSOR)] += 1
                time.sleep(0.01)
            log.log("Porta fechada: %d/10" % (res[1]))
            if res[0] > res[1]:
                return 'OPEN'
            else:
                return 'CLOSED'
    except:
        error = True
        return default

def wait_door(state,timeout):
    door_status = state
    start_time = time.time()
    while door_status == state:
        if time.time() - start_time > timeout:
            return 'timeout' 
        door_status = sensor_door(door_status)
    return 'completo'
