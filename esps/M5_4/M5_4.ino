/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-cam-video-streaming-web-server-camera-home-assistant/
  
  IMPORTANT!!! 
   - Select Board "AI Thinker ESP32-CAM"
   - GPIO 0 must be connected to GND to upload a sketch
   - After connecting GPIO 0 to GND, press the ESP32-CAM on-board RESET button to put your board in flashing mode
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#include "esp_camera.h"
#include "esp_timer.h"
#include "img_converters.h"
#include "Arduino.h"
#include "fb_gfx.h"
#include "soc/soc.h" //disable brownout problems
#include "soc/rtc_cntl_reg.h"  //disable brownout problems
#include <EEPROM.h>

//Replace with your network credentials
const char* ssid = "Appis15";
const char* password = "GDM@pp!s";

#define prateleira 4

#define PART_BOUNDARY "123456789000000000000987654321"

//#define CAMERA_MODEL_M5STACK_PSRAM
#define CAMERA_MODEL_M5STACK_WITHOUT_PSRAM

#if defined(CAMERA_MODEL_M5STACK_PSRAM)
  #define PWDN_GPIO_NUM    -1
  #define RESET_GPIO_NUM    -1
  #define XCLK_GPIO_NUM     27
  #define SIOD_GPIO_NUM     22
  #define SIOC_GPIO_NUM     23
  
  #define Y9_GPIO_NUM       19
  #define Y8_GPIO_NUM       36
  #define Y7_GPIO_NUM       18
  #define Y6_GPIO_NUM       39
  #define Y5_GPIO_NUM        5
  #define Y4_GPIO_NUM       34
  #define Y3_GPIO_NUM       35
  #define Y2_GPIO_NUM       32
  #define VSYNC_GPIO_NUM    25
  #define HREF_GPIO_NUM     26
  #define PCLK_GPIO_NUM     21

#elif defined(CAMERA_MODEL_M5STACK_WITHOUT_PSRAM)
  #define PWDN_GPIO_NUM     -1
  #define RESET_GPIO_NUM    15
  #define XCLK_GPIO_NUM     27
  #define SIOD_GPIO_NUM     25
  #define SIOC_GPIO_NUM     23
  
  #define Y9_GPIO_NUM       19
  #define Y8_GPIO_NUM       36
  #define Y7_GPIO_NUM       18
  #define Y6_GPIO_NUM       39
  #define Y5_GPIO_NUM        5
  #define Y4_GPIO_NUM       34
  #define Y3_GPIO_NUM       35
  #define Y2_GPIO_NUM       17
  #define VSYNC_GPIO_NUM    22
  #define HREF_GPIO_NUM     26
  #define PCLK_GPIO_NUM     21
  
#else
  #error "Camera model not selected"
#endif


void enviarImagem(){
  camera_fb_t * fb = NULL;
  size_t _jpg_buf_len = 0;
  uint8_t prat = prateleira;
  
  fb = esp_camera_fb_get();
  if (!fb) {
    Serial.println("Camera capture failed");
  } 
  else {
    Serial.write(&prat,1);
    Serial.write((unsigned char*)&(fb->len),4);
    //Serial.write(fb->buf,fb->len);
    for(int i = 0; i < fb->len;){
      int passo  = Serial.availableForWrite();
      Serial.write(&(fb->buf[i]),passo);
      i+= passo;
      Serial.flush();
    }
    Serial.println("\nfim de transmição");
    esp_camera_fb_return(fb);
  }    
}

void setup() {
  WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); //disable brownout detector
 
  Serial.begin(1000000);
  EEPROM.begin(12);
  int8_t brilho = EEPROM.read(0);
  int8_t saturacao = EEPROM.read(1);
  int8_t contraste = EEPROM.read(2);

  Serial.println((int)brilho);
  Serial.println((int)saturacao);
  Serial.println((int)contraste);

  Serial.println("Iniciou do setup");

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG; 
  config.frame_size = FRAMESIZE_VGA;
  config.jpeg_quality = 8;
  config.fb_count = 1;
  
  // Camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }
  sensor_t * s = esp_camera_sensor_get();
  s->set_brightness(s, (int)brilho);
  s->set_contrast(s, (int)contraste);
  s->set_saturation(s, (int)saturacao);
}

void config_params(){
  int8_t brilho = Serial.read();
  int8_t saturacao = Serial.read();
  int8_t contraste = Serial.read();
  EEPROM.write(0,brilho);
  EEPROM.write(1,saturacao);
  EEPROM.write(2,contraste);
  EEPROM.commit();
  ESP.restart();
}

void loop() {
  delay(10);

  

  if(Serial.available() > 0){
    if(Serial.read() == 'c'){
      config_params();
    }
    enviarImagem();
    while(Serial.available() > 0){
      Serial.read();
    }
  }
}
