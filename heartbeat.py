from threading import Thread
import requests
import subprocess
import json
import time 
import sys
import os

import log
import iot
import camera
import db
import uget
import door
import tickets
import paths
import image_log

reboots = db.Table(paths.jsons+'/Reboots.json')
version = 44
last_tablet_message = time.time() - 300

def check_internet():
    if not uget.current_event is None:
        return
    try:
        subprocess.run(['ping','-c1','10.200.200.1'],check=True)
    except Exception as e:
        log.log('Internet error:'+ str(e))
        if uget.current_event is None:
            os.system('reboot')
            exit()

def check_tablet():
    if time.time() - last_tablet_message > 900:
        log.log('Restartin avhai')
        os.system('systemctl restart avahi-daemon.service')
        return False
    return True

def check_door():
    porta = door.sensor_door('ERROR')
    if porta == 'OPEN' and uget.current_event is None:
        uget.api(uget.new_ticket_url,tickets.porta_aberta_inesperada())
    if uget.current_event is None:
        door.close()
    return porta

def check_ip():
    try:
        data = json.loads(subprocess.check_output(['ip','-4','-j','address']).decode('utf-8'))
        return data[1]['addr_info'][0]['local']
    except Exception as e:
        log.log(str(e))
        return None


def heartbeat(i):
    log.log('Heartbeat '+str(i))
    if not check_tablet():
        check_internet()
    cameras = uget.snap()
    image_log.save_log()
    porta = check_door()
    if porta == 'OPEN':
        uget.future_begin = False
    ip = check_ip()
    data = {'version':version,'cameras':cameras,'door_state':porta,'rasp_ip':ip}
    uget.api(uget.heartbeat_url_rust,data)
    data = {'health_details':{'version':version,'cameras':cameras,'door_state':porta,'rasp_ip':ip},'device_type':3}
    uget.api(uget.heartbeat_url,data)
    uget.solve_pendencies()
    iot.checkConn()

class Heartbeat(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.setDaemon(True)
        self.i = 0
    def run(self):
        while True:
            try:
                heartbeat(self.i)
            except Exception as e:
                log.log('Hb error:'+str(e))
            time.sleep(600)
            self.i+=1
