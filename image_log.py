from datetime import datetime
import time
import os

import shutil
import paths
import uget
import log
import db

next_clean = db.Table(paths.jsons+'/next_clean')

WEEK = 604800
DAY = 86400

def save_log():
    clean()
    now = log.timestamp()
    if time.time() - uget.last_picture < 120:
        os.mkdir(paths.path + '/image_log/' + now)
        uget.copy_images('last','../image_log/'+now,'last','img')

def clean():
    log.log('Cleaning')
    for f in os.listdir(paths.path + '/image_log'):
        try:
            date = datetime.timestamp(datetime.strptime(f,"%Y-%m-%d-%H-%M-%S"))
            if time.time() - date > 2*WEEK:
                shutil.rmtree(paths.path +'/image_log/'+f)
        except:
            os.remove(paths.path +'/image_log/'+f)
