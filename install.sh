mkdir -p /home/pi/UGET/transacoes/teste
mkdir -p /home/pi/UGET/transacoes/last
mkdir -p /home/pi/UGET/image_log
touch /home/pi/UGET/transacoes/log_geral

model=$(jq -r .CAMERA_MODEL /Config.json)
dir='/home/pi/UGET/'

i=0
while [ $i -lt 4 ]
do
	j=$(( $i+1 ))
	if [ $psram == M5_PSRAM  ]
	then
		esptool --baud 115200 --port /dev/ttyUSB$i write_flash 0x10000 $dir/esps/M5_PSRAM_$j/M5_PSRAM_$j.ino.esp32.bin
	fi
	if [ $psram == M5  ]
	then
		esptool --baud 115200 --port /dev/ttyUSB$i write_flash 0x10000 $dir/esps/M5_$j/M5_$j.ino.esp32.bin
	fi
	i=$(( $i+1 ))
done

device_name=$(jq -r .DEVICE_NAME /Config.json)
touch remoteit_in

echo "1" > remoteit_in
echo "tiagovgodoi@hotmail.com" >> remoteit_in
echo "ugetC0c4" >> remoteit_in
echo $device_name >> remoteit_in
echo "1" >> remoteit_in
echo "1" >> remoteit_in
echo "y" >> remoteit_in
echo "${device_name}_ssh" >> remoteit_in
echo "1" >> remoteit_in
echo "2" >> remoteit_in
echo "y" >> remoteit_in
echo "${device_name}_web" >> remoteit_in
echo "5" >> remoteit_in
echo "y" >> remoteit_in

connectd_installer < remoteit_in

wg_ip=$(jq -r .WG_IP /Config.json)
wg_psk=$(jq -r .WG_PSK /Config.json)
wg_key=$(jq -r .WG_KEY /Config.json)
echo "[interface]" > /etc/wireguard/wg0.conf
echo "Address = $wg_ip" >> /etc/wireguard/wg0.conf
echo "PrivateKey = $wg_key" >> /etc/wireguard/wg0.conf
echo "MTU = 1200" >> /etc/wireguard/wg0.conf
echo "" >> /etc/wireguard/wg0.conf
echo "[Peer]" >> /etc/wireguard/wg0.conf
echo "PublicKey = VF/aUEWpbvHwx3e186mVTb90yyxqnO5T0xQ57N5IUDQ=" >> /etc/wireguard/wg0.conf
echo "PresharedKey = $wg_psk" >> /etc/wireguard/wg0.conf
echo "Endpoint = 34.202.4.119:51820" >> /etc/wireguard/wg0.conf
echo "PersistentKeepalive = 25" >> /etc/wireguard/wg0.conf
echo "AllowedIPs = 10.200.200.1/16" >> /etc/wireguard/wg0.conf

wg-quick up wg0
systemctl enable wg-quick@wg0.service

pip3 install -r requirements.txt

cp /home/pi/UGET/.checkdoor.service /lib/systemd/system/checkdoor.service
systemctl enable checkdoor
systemctl start checkdoor

cp /home/pi/UGET/.dhcpcd.conf /etc/dhcpcd.conf

apt install avahi-daemon
cp /home/pi/UGET/.avahi-daemon.conf /etc/avahi/avahi-daemon.conf
systemctl enable avahi-dameon.service
systemctl start avahi-dameon.service


if [ ! -f /home/pi/UGET/usbreset ]
then 
	cc /home/pi/UGET/usbreset.c -o usbreset
	chmod +x usbreset
fi

systemctl restart server

rm remoteit_in
rm /Config.json

reboot


