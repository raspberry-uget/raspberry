from threading import Thread
import requests
import json
import time
import os

try:
    from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
    from AWSIoTPythonSDK import MQTTLib
    from datetime import datetime
    from datetime import timedelta
    from datetime import timezone
    from dateutil import parser
except: 
    os.system('pip3 install -r /home/pi/UGET/requirements.txt')
    os.system('systemctl restart server.service')

import camera
import audit
import paths
import uget
import log


#  All requests:
# 1 - take_pictures
# 2 - open_door_log
# 3 - fix_end
# 4 - swap_cams
# 5 - status
# 6 - set_cam_config
# 7 - get_cam_config
# 8 - get_event
# 9 - get_folder
# 10 - start_transaction
# 11 - start_supply
# 12 - img_log (deprecated)
# 13 - image
# 14 - file
# 15 - audit


# 1 - log (ignore)
# 2 - text_log (ignore)
# 3 - clear_config (ignore)
# 4 - usage (ignore)
# 5 - audit_tablet (ignore)
# 6 - file_tablet (ignore)
# 7 - img_tablet (ignore)


port = 443
rootCAPath = '/home/pi/UGET/iot/rootCA.crt'
privateKeyPath = '/home/pi/UGET/iot/secret.key'
certificatePath = '/home/pi/UGET/iot/certificate.pem'

host = 'a1k12qfoxtkw4j.iot.us-east-1.amazonaws.com'
if 'IOT_URL' in uget.config:
    host = uget.config['IOT_URL']

if not os.path.exists(privateKeyPath):
    headers = { 'Content-Type': 'application/json','auth':uget.config['MACHINE_TOKEN']}
    response = requests.get(uget.get_secret_url,headers=headers)
    log.log("key response: "+ str(response))
    if response.status_code == 200:
        f = open(privateKeyPath,'wb')
        f.write(response.content)
        f.close()

if not os.path.exists(certificatePath):
    headers = { 'Content-Type': 'application/json','auth':uget.config['MACHINE_TOKEN']}
    response = requests.get(uget.get_cert_url,headers=headers)
    log.log("cert response: "+ str(response))
    if response.status_code == 200:
        f = open(certificatePath,'wb')
        f.write(response.content)
        f.close()

client = None
connected = False

ignore = ['log','text_log','clear_config','get_cam_config','set_cam_config','usage','audit_tablet','file_tablet','img_tablet']

def Response(unique):
    return {
        'unique':unique,
        'status':'acknowledge',
        'data':None
    }

def send(response):
    state = {
        'state':{
            'reported':response
        }
    }
    log.log(json.dumps(state))
    client.publish('$aws/things/%d/shadow/update' % uget.config['MACHINE_ID'],json.dumps(state),0)

def deleteShadow():
    client.publish('$aws/things/%d/shadow/delete' % uget.config['MACHINE_ID'],None,0)



class HandleThread(Thread):
    def __init__(self,message):
        Thread.__init__(self)
        self.message = message
    def run(self):
        try:
            formatted = json.loads(self.message.payload)
            if formatted['command'] in ignore:
                return
            log.log('Message Recieved' + str(self.message.payload))
            deleteShadow()
            response = Response(formatted['unique'])
            send(response)
            data = interpretCommand(formatted)
            if not data is None:
                response['status'] = 'completed'
                response['data'] = data
            else:
                response['status'] = 'not_found'
            send(response)
        except Exception as e:
            log.log('Message error: '+str(e))
        
class Connect(Thread):
    def run(self):
        global connected
        try:
            clientId = str(uget.config['MACHINE_ID'])
            client.connect()
            client.subscribe('commands/'+clientId,0,handle)
            connected = True
            log.log('iot subscribed')
        except Exception as e:
            log.log('Iot connection error: '+str(e))

def handle(client,userdata,message):
    ht = HandleThread(message)
    ht.start()

def setup():
    global client
    try:
        clientId = str(uget.config['MACHINE_ID'])

        client = AWSIoTMQTTClient(clientId)
        client.configureEndpoint(host, port)
        client.configureCredentials(rootCAPath, privateKeyPath, certificatePath)
        client.configureAutoReconnectBackoffTime(1, 32, 20)
        client.configureConnectDisconnectTimeout(10)
        client.configureMQTTOperationTimeout(5) 
        client.configureOfflinePublishQueueing(1,MQTTLib.DROP_OLDEST)

        c = Connect()
        c.start()

    except Exception as e:
        log.log("Connecting error: " + str(e))

def checkConn():
    if not connected:
        c = Connect()
        c.start()


def uploadTemp(key,path):
    try:
        if path.startswith('Transaction'):
            path = path.replace('Transaction','transacoes')
        url = uget.upload_temp_url % key
        headers = { 'auth':uget.config['MACHINE_TOKEN'],'accept':'application/json'}
        path = paths.path + '/' + path
        body = open(path,'rb').read()
        requests.put(url,headers=headers,data=body,timeout=60)
    except Exception as e:
        log.log('Upload temp error: ' + str(e))


def toISO(timestamp):
    utc = timezone(timedelta(0))
    date = datetime.strptime(timestamp,"%Y-%m-%d-%H-%M-%S").astimezone(utc)
    return date.isoformat()

def toTimestamp(iso):
    date = parser.isoparse(iso)
    return date.strftime("%Y-%m-%d-%H-%M-%S") 

def interpretCommand(message):
    command = message['command']

    if command == 'take_pictures':
        return {'pictures':uget.test_pictures()}

    if command == 'get_event':
        folder = message['data']['folder']
        return {'event':uget.get_event(folder)}

    if command == 'get_folder':
        event = message['data']['event']
        return {'folder':uget.get_folder(event)}

    if command == 'start_supply':
        return {'status':'Not supported by machine'}

    if command == 'start_transaction':
        return {'status':'Not supported by machine'}

    if command == 'swap_cams':
        new = message['data']['new_value']
        current = message['data']['current_value']
        camera.swap_shelves(current,new)
        return {'status':'ok'}

    if command == 'fix_end':
        event = message['data']['event']
        folder = uget.get_folder(event)
        if folder is None:
            return {'status':'Event not found in this machine'}
        resultado = uget.fix_end(folder)
        if resultado == 'ok':
            if not uget.upload_aws(folder,event,None):
                resultado = 'fixend worked, but reupload failed'
        return {'status':resultado}

    if command == 'status':
        return uget.machineStatus()

    if command == 'open_door_log':
        begin = toTimestamp(message['data']['begin_time'])
        end = toTimestamp(message['data']['end_time'])
        limit = message['data']['limit']
        openDoor = audit.find_interval(begin,end,limit)
        openDoor = list(map(toISO,openDoor))
        return {'openings':openDoor}

    if command == 'image':
        key = message['data']['key']
        paths = message['data']['paths']
        i = 1
        for path in paths:
            uploadTemp("%s%%2F%d.jpg" % (key,i),path)
            i=i+1
        return {'status':'ok'}

    if command == 'audit':
        sub = message['data']['path']
        l = audit.list_dir(sub) 
        if l is None:
            return {'list':None,'size':0}
        page = message['data']['page']
        limit = message['data']['limit']
        begin = page*limit
        end = min(begin + limit,len(l))
        if begin > len(l):
            return {'list':None,'size':len(l)}
        subl = l[begin:end]
        return {'list':subl,'size':len(l)}

    if command == 'file':
        key = message['data']['key']
        paths = message['data']['paths']
        i = 1
        for path in paths:
            uploadTemp("%s%%2F%d.txt" % (key,i),path)
            i=i+1
        return {'status':'ok'}

    if command == 'wg':
        ip = message['data']['ip']
        key = message['data']['key']
        psk = message['data']['psk']
        try:
            os.system('bash /home/pi/UGET/new_wg.sh %s %s %s' % (ip,key,psk))
            return {'status':'ok'}
        except Exception as e:
            return {'status':str(e)}
