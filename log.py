from datetime import datetime

import paths

log_file = None

def timestamp():
    return datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

def log(msg):
    full_msg = '%s: %s\n' % (timestamp(),msg)
    print(full_msg,end='')
    if log_file != None and not log_file.closed:
        log_file.write(full_msg)
        log_file.flush()
    else:
        f = open(paths.transacoes+'/log_geral','a')
        f.write(full_msg)
        f.close()

def open_log(event_id):
    global log_file
    log_file = open(paths.transacoes+'/'+event_id+'/log.txt','w')

def close():
    global log_file
    log_file.close()
