#!/bin/bash

if [ $# -ne 3 ]
then
	exit
fi

echo "[interface]" > /etc/wireguard/wg0.conf
echo "Address = $1" >> /etc/wireguard/wg0.conf
echo "PrivateKey = $2" >> /etc/wireguard/wg0.conf
echo "MTU = 1200" >> /etc/wireguard/wg0.conf
echo "" >> /etc/wireguard/wg0.conf
echo "[Peer]" >> /etc/wireguard/wg0.conf
echo "PublicKey = VF/aUEWpbvHwx3e186mVTb90yyxqnO5T0xQ57N5IUDQ=" >> /etc/wireguard/wg0.conf
echo "PresharedKey = $3" >> /etc/wireguard/wg0.conf
echo "Endpoint = 34.202.4.119:51820" >> /etc/wireguard/wg0.conf
echo "PersistentKeepalive = 25" >> /etc/wireguard/wg0.conf
echo "AllowedIPs = 10.200.200.1/16" >> /etc/wireguard/wg0.conf

wg-quick down wg0
wg-quick up wg0
