path = '/home/pi/UGET'
transacoes = path + '/transacoes'
jsons = path + '/jsons'

import os 

def create_folder_if_not_exists(folder_path):
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

def create_file_if_not_exists(file_path):
    if not os.path.exists(file_path):
        with open(file_path, 'w') as file:
            pass

try:
    create_folder_if_not_exists(path)
    create_folder_if_not_exists(transacoes)
    create_folder_if_not_exists(jsons)
    create_folder_if_not_exists(path+'/image_log')
    create_folder_if_not_exists(transacoes+'/last')
    create_folder_if_not_exists(transacoes+'/teste')
    create_file_if_not_exists(transacoes+'/log_geral')
except:
    pass




