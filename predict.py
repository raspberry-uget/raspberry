import json
from scipy.spatial import distance
import os
import numpy as np
import requests
import cv2

def calc_dist(begin_rgb, end_rgb):
	begin = cv2.cvtColor(begin_rgb, cv2.COLOR_RGB2LAB)
	end = cv2.cvtColor(end_rgb, cv2.COLOR_RGB2LAB)
	l_begin = (np.mean(begin[:,:,0].flatten())/255.0)
	a_begin = (np.mean(begin[:,:,1].flatten())-128)/128.0
	b_begin = (np.mean(begin[:,:,2].flatten())-128)/128.0
	l_end = (np.mean(end[:,:,0].flatten())/255.0)
	a_end = (np.mean(end[:,:,1].flatten())-128)/128.0
	b_end = (np.mean(end[:,:,2].flatten())-128)/128.0
	dst = distance.euclidean((l_begin,a_begin,b_begin), (l_end,a_end,b_end))
	return dst

def hist_match(source_all, template_all):
    result = np.zeros_like(source_all)

    for c in range(3):
        source = source_all[:, :, c]
        template = template_all[:, :, c]
        oldshape = source.shape
        source = source.ravel()
        template = template.ravel()

        s_values, bin_idx, s_counts = np.unique(source, return_inverse=True, return_counts=True)
        t_values, t_counts = np.unique(template, return_counts=True)

        s_quantiles = np.cumsum(s_counts).astype(np.float64)
        s_quantiles /= s_quantiles[-1]
        t_quantiles = np.cumsum(t_counts).astype(np.float64)
        t_quantiles /= t_quantiles[-1]

        interp_t_values = np.interp(s_quantiles, t_quantiles, t_values)

        result[:, :, c] =  interp_t_values[bin_idx].reshape(oldshape)

    return result

def compare_lab(begin, end, shelf, out_of_stock, thresh):
    shop_ids = []
    shop_pos = []
    for row in shelf:
        for pos in row:
            if not pos['index'] in out_of_stock:
                x,y,w,h = pos['mask']
                piece_begin = begin[y:y+h, x:x+w].copy()
                piece_end = end[y:y+h, x:x+w].copy()
                dst = calc_dist(piece_begin, piece_end)
                if dst >= thresh:
                    shop_ids.append(pos['id'])
                    shop_pos.append(pos['index'])
                    out_of_stock.append(pos['index'])
    return shop_ids, shop_pos, out_of_stock

def predict_local(path,log_file,config):
    json_counts = []
    ids = []
    pos = []    
    try:
        thresh = config['MODEL_THRESH']
        out_of_stock = {}
        id_map = {}
        stock = {};

        try:
            out_of_stock = json.load(open('out_of_stock.json', 'r'))
            id_map = json.load(open('id_map.json', 'r'))
        except:
            log_file.write('Sem arquivos de stock locais');

        try:
            headers = {
                'Content-Type': 'application/json',
                'auth': config['MACHINE_TOKEN']
            }
            log_file.write('consultou back arq\n')
            resp_stock = requests.get('https://api.uget.express/machine/stock', headers=headers)
            resp_out_of_stock = requests.get('https://api.uget.express/machine/out_of_stock', headers=headers)

            stock = json.loads(resp_stock.json()['stock'])
            out_of_stock = json.loads(resp_out_of_stock.json()['out_of_stock'])
            log_file.write('leu o back\n')
            json.dump(stock, open('id_map.json', 'w'))
            json.dump(out_of_stock, open('out_of_stock.json', 'w'))
            log_file.write('salvou o arq\n')
        except Exception as e:
            log_file.write("erro no pre-predict",str(e) + '\n')

        for cam in range(1,5):
            begin_file = path + 'begin{}.jpg'.format(cam)
            end_file = path + 'end{}.jpg'.format(cam)

            if os.path.isfile(begin_file) and os.path.isfile(end_file):
                im_begin = cv2.imread(begin_file)
                source = cv2.imread(end_file)
                template = cv2.imread(begin_file)
                im_end = hist_match(source,template)

                shop_ids, shop_pos, out_of_stock = compare_lab(im_begin, im_end, stock[cam-1], out_of_stock, thresh)
                ids = ids + shop_ids
                pos = pos + shop_pos
            else:
                log_file.write('Predict - não econtrou ' +  begin_file + ' ou ' + end_file +'\n');


        unique, counts = np.unique(np.array(ids), return_counts=True)
        for i, sku in enumerate(unique):
            qt = counts[i]
            json_counts.append({"code": str(sku), "quantity": str(qt)})

        json.dump(out_of_stock, open('out_of_stock.json', 'w'))
        return json_counts, pos
    except Exception as e:
        log_file.write('erro predict: ')
        log_file.write(str(e))
        return json_counts, pos
