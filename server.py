from flask import Flask, request, Response, jsonify, send_from_directory, redirect, url_for, send_file
from threading import Thread
from threading import Semaphore
from PIL import Image
from PIL import ImageFile
import subprocess
import requests
import os
import sys
import io
import json
import time 
import shutil 
import base64

import log
import paths
import camera
import db
import uget
import door
import heartbeat
#import predict
import audit
import image_log
import iot

app = Flask(__name__)

# Retorna o status para o Tablet acompanhar a transação
# status = 1 iniciou captura do begin
# status = 5 porta ainda fechada
# status = 6 porta nao abriu, finaliza transacao
# status = 7 porta abriu e mandou comando pra fechar trava
# status = 10 porta fechou e inicia captura das imagens do end
# status = 11 predict e buy feito, mostra o predict
# status = 12 end capturado mas sem predict, não mostra o predict
# status = 13 upload errado, volta pro inicio no tablet
# status = 14 upload ok, volta pro inicio no tablet

@app.route('/status', methods=['GET'])
def respond_status():
    if not uget.current_event is None:
        log.log('status : ' + str(uget.current_event.status))
        if uget.current_event.status == 11:
            return json.dumps({'status': uget.current_event.status,'value':uget.current_event.predicted_value})
        status = uget.current_event.status
        return json.dumps({'status': status})
    log.log('status: 100')
    return json.dumps({'status': 100})
        

@app.route('/detection_time', methods=['POST'])
def read_time():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    data = request.get_json()
    log.log(str(data))
    uget.face_detection = data['face_detection']
    uget.check_user = data['check_user']
    return json.dumps({'response': 'ok'})

# request_body: {'event_id': 123, 'is_buy': False}
# response_body {'msg': 'ok'}

@app.route('/begin', methods=['POST'])
def begin_handler():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    if not uget.current_event is None and uget.current_event.status < 11:
        return 'Busy', 425
    data = request.get_json()
    log.log('Begin request: '+str(data))
    event_id = data['event_id']
    supply = data['supplier']
    uget.current_event = uget.Transaction(supply,event_id,None,None)
    uget.current_event.start()
    return json.dumps({'response': 'ok'})

@app.route('/begin_offline', methods=['POST'])
def begin_offline():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    if not uget.current_event is None and uget.current_event.status < 11:
        return 'Busy', 425
    data = request.get_json()
    log.log('Begin request: '+str(data))
    user = data['user']
    qr_msg = data['qr_message']
    supply = False
    if 'supplier' in data:
        supply = data['supplier']
    uget.current_event = uget.Transaction(supply,None,user,qr_msg)
    uget.current_event.start()
    return json.dumps({'response': 'ok'})


@app.route('/', methods=['GET'])
def setup():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    uget.snap()
    html = open(paths.path+'/htmls/index.html').read()
    random = str(time.time())
    html = html % (random,random,random,random) 
    return html

@app.route('/take_picture', methods=['POST'])
def snap():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    sucesso = uget.test_pictures()
    return json.dumps({'sucesses':sucesso})

@app.route('/teste30', methods=['GET'])
def t30():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    try:
        shutil.rmtree(paths.transacoes+'/teste30')
    except:
        pass
    os.mkdir(paths.transacoes+'/teste30')
    for i in range(30):
        camera.capture_images('teste'+str(i)+'_','teste30')
    html = open(paths.path+'/htmls/teste30.html').read()
    return html

@app.route('/transacoes/<path:path>')
def send_img(path):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    if str(path).startswith('teste/teste'):
        uget.snap()
    return send_from_directory(paths.transacoes, path)

@app.route('/jsons/<path:path>')
def send_json(path):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    if str(path).startswith('teste/teste'):
        uget.snap()
    return send_from_directory(paths.jsons, path)

@app.route('/reupload/<event_id>')
def send_images_again(event_id):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    if not event_id in uget.events.get('events',[]):
        return 'Event not found in this machine'
    index = uget.events.get('events',[]).index(event_id)
    folder = uget.events.get('folders',[])[index]
    resultado = uget.upload_aws(folder,event_id,None)
    log.log(str(resultado))
    return json.dumps({'sucess': resultado})

@app.route('/fixend/<event_id>')
def fix_end_handle(event_id):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    log.log('Consertando end:'+event_id)
    folder = uget.get_folder(event_id)
    if folder is None:
        return 'Event not found in this machine'
    resultado = uget.fix_end(folder)
    if resultado == 'ok':
        if not uget.upload_aws(folder,event_id,None):
            resultado = 'fixend worked, but reupload failed'
    log.log(str(resultado))
    return json.dumps({'response': resultado})

@app.route('/fixbegin/<event_id>')
def fix_begin_handle(event_id):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    log.log('Consertando begin:'+event_id)
    folder = uget.get_folder(event_id)
    if folder is None:
        return 'Event not found in this machine'
    resultado = uget.fix_begin(folder)
    if resultado == 'ok':
        if not uget.upload_aws(folder,event_id,None):
            resultado = 'fixend worked, but reupload failed'
    log.log(str(resultado))
    return json.dumps({'response': resultado})

@app.route('/folder/<event_id>')
def folder_handle(event_id):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    return json.dumps({'folder':uget.get_folder(event_id)})

@app.route('/event/<folder>')
def event_handle(folder):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    return json.dumps({'event':uget.get_event(folder)})

git_mutex = Semaphore()

@app.route('/update',methods=['POST'])
def update():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    uget.mutex.acquire()
    uget.mutex.release()
    git_mutex.acquire()
    os.system('bash /home/pi/UGET/update.sh')
    git_mutex.release()
    return json.dumps({'response': 'error'})

@app.route('/version',methods=['GET'])
def version_hanlder():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    heartbeat.last_tablet_message = time.time()
    if time.time() - uget.last_picture > 600:
        s = uget.Snap()
        s.start()
    return json.dumps({'version': heartbeat.version,'last_picture':time.time() - uget.last_picture})

@app.route('/door',methods=['GET'])
def door_handler():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    html = open('/home/pi/UGET/htmls/door.html').read()
    door_state = door.sensor_door('offline')
    html = html % (door_state) 
    return html

@app.route('/door/open',methods=['GET'])
def open_hanlder():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    door.open()
    return redirect(url_for('door_handler'))

@app.route('/door/close',methods=['GET'])
def close_hanlder():
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    door.close()
    return redirect(url_for('door_handler'))

@app.route('/cams/<port>',methods=['GET','POST'])
def port_hanlder(port):
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    if request.method != 'GET':
        try:
            prat = camera.get_shelf(port)
            os.remove(paths.transacoes+'/teste/teste'+prat+'.jpg')
        except:
            pass
        camera.set_cam_config(port,dict(request.get_json()))

    start_time = time.time()
    uget.test_pictures()
    delta_time = time.time() - start_time

    prat = camera.get_shelf(port)
    html = open('/home/pi/UGET/htmls/cam_config.html').read()
    random = str(time.time())
    machine_id = uget.config['MACHINE_ID']
    inv = int(camera.invert[int(prat)])
    dim = str(camera.width)+':'+str(camera.height)
    if uget.config['CAMERA_MODEL'] == 'USB': 
        settings = camera.get_cam_config(port) 
        next_port =(int(port) + 2) % 8 
        html = html % (machine_id,random,prat,delta_time,prat,'visible',int(camera.ramps[int(prat)]),inv,settings,machine_id,next_port,dim)
    else:
        next_port = str(int(port)%4 + 1)
        html = html % (machine_id,random,prat,delta_time,prat,dim,'hidden',int(camera.ramps[int(prat)]),inv,"",machine_id,next_port,dim)
    return html

@app.route('/image/<random>/<prat>')
@app.route('/shelf/<prat>')
def image_handler(prat,random=None): 
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    return send_file(paths.transacoes+'/teste/teste'+prat+'.jpg',mimetype = 'image/jpeg')

@app.route('/all_images')
def all_images(): 
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    uget.snap()
    path = paths.transacoes+'/teste/'  
    pictures = []
    for i in range(1,5):
        try:
            file_path = os.path.join(path, 'teste'+str(i)+'.jpg')
            if os.path.exists(file_path):
                imageObject = Image.open(file_path)
                buffered = io.BytesIO()
                imageObject.save(buffered, format="JPEG")
                base64_string = base64.b64encode(buffered.getvalue()).decode('utf-8')
                pictures.append(base64_string)
            else:
                log.log(file_path + ' não existe')
                pictures.append('')
        except Exception as e:
            pictures.append('')
            log.log(str(e))
    return json.dumps({'pictures':pictures})


@app.route('/reset_cam')
def reset_cam(): 
    log.log('Request ' + str(request.path) + ' from ' + str(request.remote_addr))
    camera.check_hub()
    cams = uget.snap()
    return json.dumps({'cameras':cams})

@app.route('/audit/<path:path>')
def handle_audit_path(path): 
    if request.args.get('page') is None or request.args.get('limit') is None:
        return json.dumps({"error":"missing parameters"}), 400
    start = int(request.args.get('page')) * int(request.args.get('limit'))
    end = (int(request.args.get('page')) + 1) * int(request.args.get('limit'))
    dirs = audit.list_dir(path)[start:end]
    return json.dumps({"files":dirs})

@app.route('/audit/')
def handle_audit(): 
    if request.args.get('page') is None or request.args.get('limit') is None:
        return json.dumps({"error":"missing parameters"}), 400
    start = int(request.args.get('page')) * int(request.args.get('limit'))
    end = (int(request.args.get('page')) + 1) * int(request.args.get('limit'))
    dirs = audit.list_dir('')[start:end]
    return json.dumps({"files":dirs})

@app.route('/open_door_log')
def handle_door_log(): 
    max_len = None
    if request.args.get('begin') is None or request.args.get('end') is None:
        return json.dumps({"error":"missing parameters"}), 400
    start = request.args.get('begin')
    end = request.args.get('end')
    try:
        if request.args.get('max') is not None:
            max_len = int(request.args.get('max'))
    except:
        return json.dumps({"error":"invalid parameters"}), 400

    if start > end:
        return json.dumps({"error":"begin bigger than end"}), 400
    openDoor = audit.find_interval(start,end,max_len)
    return json.dumps({"OpenDoor":openDoor})

if __name__ == '__main__':
    log.log('INICIANDO O SERVIDOR')

    log.log(subprocess.check_output('iwconfig').decode('utf-8'))

    uget.setup()
    door.setup()
    camera.setup()
    iot.setup()

    heartbeat.Heartbeat().start()
    
    app.run(debug=True, host='::', port=80,use_reloader=False)
