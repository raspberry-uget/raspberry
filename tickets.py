import uget
import time
from datetime import timezone
from datetime import datetime
from datetime import timedelta

def signed_ticket(ticket,fullstring,folder,customer):
    utc = timezone(timedelta(0))
    folder_time = datetime.strptime(folder,"%Y-%m-%d-%H-%M-%S")
    ticket['customer_id'] = customer
    ticket['qr'] = fullstring
    ticket['is_offline'] = False
    ticket['creation'] =  folder_time.astimezone(utc).strftime("%Y-%m-%d %H:%M:%S.%f") + 'Z'
    ticket['current_time'] = str(datetime.utcfromtimestamp(time.time())) + 'Z'

def porta_nao_abriu(event):
    return {
        "queue_id":14,
        "title": 'Não abriram a porta',
        "description":'O cliente não abriu a porta em 10 segundos no evento ' + str(event),
    }

def porta_nao_fechou(event):
    return {
        "queue_id":15,
        "title": 'Não fecharam a porta',
        "description":'O cliente não fechou a porta em 2 minutos no evento ' + str(event),
    }

def erro_begin(event,cameras):
    return {
        "queue_id":16,
        "title": 'Erro de câmeras',
        "description":'O evento %s falhou porque só tirou %d fotos no begin.' % (event,cameras),
    }

def erro_end(event,cameras):
    return {
        "queue_id":17,
        "title": 'Imagens faltando',
        "description":'No evento %s só foram capturadas %d fotos no end.' % (event,cameras),
    }

def erro_upload(event):
    return {
        "queue_id":18,
        "title":'Não subiram as imagens',
        "description":'Erro no upload das imagens do evento %s.' % (event),
    }

def porta_aberta_inesperada():
    return {
        "queue_id":54,
        "title":'Porta aberta inesperada',
        "description":'A porta estava aberta, mas não estava acontencendo nehuma transação.'
    }

def erro_na_criacao_de_transacao(event,msg):
    return {
        "queue_id":55,
        "title":'Erro na criação de transação',
        "description":'Houve um erro na criação da transação do evento %s. \n%s' % (event,msg)
    }
