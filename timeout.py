from threading import Thread
from threading import Semaphore
import time

class Timeout():
    def __init__(self,function, wait, default,args):
        self.function = function
        self.wait = wait
        self.value = default
        self.args = args
        
    def main_task(self):
        self.value = self.function(self.args)
        self.mutex.release()

    def timer_task(self):
        time.sleep(self.wait)
        self.mutex.release()

    def run(self):
        self.mutex = Semaphore()
        self.mutex.acquire()
        t1 = Thread(target=self.main_task)  
        t1.start()
        t2 = Thread(target=self.timer_task)  
        t2.start()
        self.mutex.acquire()
        return self.value
        
        
        
