from threading import Thread
from threading import Semaphore
from datetime import datetime
from datetime import timedelta
from datetime import timezone
from PIL import Image
from PIL import ImageFile
import requests
import shutil
import time
import os
import io
import base64
import json

from timeout import Timeout
import log
import door
import db
import paths
import camera

pendencies = db.Table(paths.jsons+'/Pendencies.json')
events = db.Table(paths.jsons+'/Events.json')
config = db.Table(paths.path+'/Config.json').all()
face_detection = None
check_user = None

last_unique_event = 0

import tickets

mutex = Semaphore()

host = 'https://api.bengelado.cocacola.com.br:8443/uget'
host_2 = 'https://api2.uget.express'

if 'HOST_URL' in config:
    host = config['HOST_URL']

if 'HOST_2_URL' in config:
    host_2 = config['HOST_2_URL']


heartbeat_url = host + '/machine/heartbeat'
heartbeat_url_rust = host_2 + '/machine/lastheartbeat/rasp'
send_telegram_url = host + '/send_telegram'
upload_images_url = host + '/machine/upload_images'
buy_url = host + '/buy_machine'
buy_signed_url = host + '/buy_signed_qr'
signed_supply_url = host + '/service_signed_qr'
register_times_url = host + '/register_times'
new_ticket_url = host + '/helpdesk/newticket_machine'
signed_ticket_url = host + '/ticket_signed_qr'
upload_temp_url =  host_2 + '/iot/upload_temp/%s'
get_secret_url =  host_2 + '/machine/policy/secret'
get_cert_url =  host_2 + '/machine/policy/cert'

current_event = None
last_picture = 0
future_begin = True

def setup():
    if not 'PREDICT_LOCAL' in config:
        config['PREDICT_LOCAL'] = False


def api(url,data):
    try :
        json_data = json.dumps(data)
        if len(json_data) < 250:
            log.log('Enviando :' +  json_data)
        else:
            log.log('Enviando :' +  json_data[0:147] +'...')
        headers = { 'Content-Type': 'application/json','auth':config['MACHINE_TOKEN'],'accept':'application/json'}
        response = requests.post(url,headers=headers,data=json_data,timeout=60)
        log.log('Resposta :' + str(response))
        log.log('Conteúdo :' + str(response.content))
        return response.status_code == 200, response
    except Exception as e:
        log.log('Erro no request:' +  str(e))
        return False, str(e)

def machineStatus():
    return {
        'event_id': int(current_event.id) if current_event is not None else None,
        'event_happening': current_event is not None,
        'door_status': door.sensor_door('ERROR')
    }


def snap(begin = False):
    global last_picture
    global future_begin
    camera.cap_mutex.acquire()
    for i in range(1,5):
        try:
            os.remove(paths.transacoes+'/teste/teste'+str(i)+'.jpg')
        except:
            log.log('Nao havia arquivo teste'+str(i)+' para remover');
    sucessos = camera.capture_images('teste','teste',slow=not begin)
    if sucessos == 4:
        if move_images('teste','last','teste','last') == 4:
            last_picture = time.time()
        if (current_event is None or not current_event.after_begin) and door.sensor_door('') == 'CLOSED':
            future_begin = True
        camera.cap_mutex.release()
    else:
        camera.cap_mutex.release()
        camera.check_hub()
    return sucessos

def test_pictures():
    camera.cap_mutex.acquire()
    for i in range(1,5):
        try:
            os.remove(paths.transacoes+'/teste/teste'+str(i)+'.jpg')
        except:
            log.log('Nao havia arquivo teste'+str(i)+' para remover');
    sucessos = camera.capture_images('teste','teste')
    camera.cap_mutex.release()
    return sucessos

def solve_pendencies():
    if current_event is not None:
        log.log('Do not solve pendencies during transaction.')
        return
    pend = pendencies.all()
    for key in pend.keys():
        try:
            log.log('solving pendency:' + str(key))
            p = pend[key]
            if 'event_id' in p.keys():
                p['event'] = p['event_id']
            if not 'buy' in p.keys() or not 'images' in p.keys() or not 'end' in p.keys():
                pendencies.update(key,None)
                continue
            if p['buy'] and p['images'] and p['end']:
                pendencies.update(key,None)
            if not p['buy']:
                if p['user'] is None:
                    buy(key,p['event'],[],[])
                else:
                    buy_signed(key,p['user'],p['qr'],True)
            if p['buy'] and p['event'] is None:
                signed_supply(key,p['user'],p['qr'],True)
            if not p['end']:
                fix_end(key)
                upload_aws(key,p['event'],None)
            if not p['images']:
                upload_aws(key,p['event'],None)
        except Exception as e:
            log.log('Erro na pendencia' + str(e))

def send_alert(error,urgent):
    log.log(error)
    data = {'msg': error}
    now = datetime.now()
    if (now.hour < 20 and now.hour > 7) or urgent:
        api(send_telegram_url,data)

def send_ticket(ticket,event_id,fullstring,folder,customer):
    if event_id is None:
        tickets.signed_ticket(ticket,fullstring,folder,customer)
        status,response = api(signed_ticket_url,ticket)
        try:
            event_id = response.json()['event_id']
            update_event_list(folder,event_id)
            return event_id
        except:
            return None
    else:
        ticket['event_id'] = int(event_id)
        api(new_ticket_url,ticket)
        return event_id

def upload_aws (folder,event_id,buy_data):
    if event_id is None:
        return False
    path = paths.transacoes+'/' + folder
    picture_name = ['begin1.jpg', 'end1.jpg', 'begin2.jpg', 'end2.jpg', 'begin3.jpg', 'end3.jpg', 'begin4.jpg', 'end4.jpg']
    pictures = []
    names = []
    for pic in picture_name:
        try:
            file_path = os.path.join(path, pic)
            if os.path.exists(file_path):
                imageObject = Image.open(file_path)
                buffered = io.BytesIO()
                imageObject.save(buffered, format="JPEG")
                base64_string = base64.b64encode(buffered.getvalue()).decode('utf-8')
                pictures.append(base64_string)
                names.append(pic)
            else:
                log.log(file_path + ' não existe.');
        except Exception as e:
            log.log(str(e))
    data = {}
    if buy_data != None:
        log.log(buy_data)
        data = {'event_id': event_id, 'picture_name': names, 'pictures': pictures,'info_buy':buy_data}
    else:
        data = {'event_id': event_id, 'picture_name': names, 'pictures': pictures}
    sucesso, response = api(upload_images_url,data)
    if sucesso:
        pendencies.update(folder,{'images':True})
        return True
    return False

def buy(folder,event_id,product_counts,pos):
    buy_id = 0
    data = {'event_id':str(event_id), 'shop':pos,'items':product_counts}
    status,response = api(buy_url,data)
    try:
        if response.content.decode('utf-8') == '{"error":{"code":0,"msg":"BadRequestException"}}':
            #Essa transação já existe, remove pendencia e ignora
            pendencies.update(folder,{'buy':True})
            return 0,data
        shopped = response.json()
        if status and 'buy_id' in  shopped:
            buy_id = shopped['buy_id']
            pendencies.update(folder,{'buy':True})
        else:
            msg = 'Resposta: ' + response.content.decode('utf-8')
            send_ticket(tickets.erro_na_criacao_de_transacao(event_id,msg),event_id,None,None,None)
        if not current_event is None:
            current_event.predicted_value = shopped['total']
        return buy_id,data
    except Exception as e:
        send_ticket(tickets.erro_na_criacao_de_transacao(event_id,response),event_id,None,None,None)
        return 0, data

def signed_supply(folder,user,qr,offline):
    buy_id = 0
    event_id = None
    utc = timezone(timedelta(0))
    folder_time = datetime.strptime(folder,"%Y-%m-%d-%H-%M-%S")
    event_type = 2 if ('OPEN_MAINTENANCE' in qr) else 1
    data = {
        'user_id':str(user),
        'qr':qr,
        'creation': folder_time.astimezone(utc).strftime("%Y-%m-%d %H:%M:%S.%f") + 'Z',
        'current_time': str(datetime.utcfromtimestamp(time.time())) + 'Z',
        'is_offline':offline,
        'event_type': event_type
    }
    status,response = api(signed_supply_url,data)
    try:
        response = response.json()
        if status and 'event_id' in response:
            event_id = response['event_id']
            pendencies.update(folder,{'event':event_id})
            return response['event_id']
        return None
    except:
        return None


def buy_signed(folder,user,qr,offline):
    buy_id = 0
    event_id = None
    utc = timezone(timedelta(0))
    folder_time = datetime.strptime(folder,"%Y-%m-%d-%H-%M-%S")
    data = {
        'customer_id':str(user),
        'qr':qr,
        'creation': folder_time.astimezone(utc).strftime("%Y-%m-%d %H:%M:%S.%f") + 'Z',
        'current_time': str(datetime.utcfromtimestamp(time.time())) + 'Z',
        'is_offline':offline
    }
    status,response = api(buy_signed_url,data)
    try:
        shopped = response.json()
        if status and 'event_id' in shopped and 'buy_id' in shopped:
            buy_id = shopped['buy_id']
            event_id = shopped['event_id']
            pendencies.update(folder,{'event':event_id,'buy':True})
            update_event_list(folder,event_id) 
        if not current_event is None:
            current_event.predicted_value = shopped['total']
        return event_id,buy_id,data
    except Exception as e:
        log.log(str(e))
        return None, 0, data

def register_times(buy_id, times):
    global face_detection
    global check_user
    not_null = 0.0 if check_user is None else check_user
    not_null += 0.0 if face_detection is None else face_detection
    times_data = {
        "buy_id":buy_id,
        "face_detection":face_detection,
        "check_user":check_user,
        "door_opened":times[2]-times[1],
        "record_transaction":times[3]-times[0],
        "predict_local": times[4] - times[3],
        "buy": times[5] - times[4],
        "upload_aws": times[6] - times[5],
        "transaction": times[6] - times[0] + not_null,
        "machine_id": config['MACHINE_ID']
    }
    face_detection = None
    check_user = None
    api(register_times_url,times_data)

def copy_images(event_src,event_dst,moment_src,moment_dst):
    camera.cap_mutex.acquire()
    copiadas = 0
    for i in range(1,5):
        try:
            src_path = paths.transacoes+'/'+event_src+'/'+moment_src+str(i)+'.jpg'
            dst_path = paths.transacoes+'/'+event_dst+'/'+moment_dst+str(i)+'.jpg'
            shutil.copy(src_path,dst_path)
            copiadas += 1
        except Exception as e:
            log.log('Erro ao copiar imagem:'+str(e))
    camera.cap_mutex.release()
    return copiadas

def move_images(event_src,event_dst,moment_src,moment_dst):
    copiadas = 0
    for i in range(1,5):
        try:
            src_path = paths.transacoes+'/'+event_src+'/'+moment_src+str(i)+'.jpg'
            dst_path = paths.transacoes+'/'+event_dst+'/'+moment_dst+str(i)+'.jpg'
            shutil.move(src_path,dst_path)
            copiadas += 1
        except Exception as e:
            log.log('Erro ao mover imagem:'+str(e))
    return copiadas

def fix_end_event(event_id):
    if not event_id in events.get('events',[]):
        return 'Event not found in this machine'
    index = events.get('events',[]).index(event_id)
    folder = events.get('folders',[])[index]
    return fix_end(folder)

def fix_end(folder):
    folders = events.get('folders',[])
    index = folders.index(folder)
    copy_images(str(folder),str(folder),'end','end'+log.timestamp()) 
    next_pos = index + 1
    if next_pos == len(folders):
        sucessos = snap()
        if sucessos != 4:
            return 'Failed taking new photos'
        if copy_images('last',str(folder),'last','end') == 4:
            pendencies.update(folder,{'end':True,'images':False})
            return 'ok'
        return 'Failed coping images'
    else:
        next_folder = folders[next_pos]
        if copy_images(next_folder,folder,'begin','end') == 4:
            pendencies.update(folder,{'end':True,'images':False})
            return 'ok'
        pendencies.update(folder,{'end':True})
        return 'Failed coping images'

def fix_begin_event(event_id):
    if not event_id in events.get('events',[]):
        return 'Event not found in this machine'
    index = events.get('events',[]).index(event_id)
    folder = events.get('folders',[])[index]
    return fix_begin(folder)

def fix_begin(folder):
    index = events.get('folders',[]).index(folder)
    copy_images(str(folder),str(folder),'end','bkp') 
    before_pos = index - 1
    if index != 0:
        before_folder = events.get('folders',[])[before_pos]
        if copy_images(before_folder,folder,'end','begin') == 4:
            return 'ok'
        else:
            return 'Failed coping images'

def update_event_list(folder,event):
    index = events.get('folders',[]).index(folder)
    event_list = events.get('events',[])
    event_list[index] = str(event)
    events.update('events',event_list)

def get_folder(event_id): 
    if not event_id in events.get('events',[]):
        return None
    index = events.get('events',[]).index(event_id)
    return events.get('folders',[])[index]

def get_event(folder): 
    if not folder in events.get('folders',[]):
        return None
    index = events.get('folders',[]).index(folder)
    return events.get('events',[])[index]

class Snap(Thread):
    def run(self):
        snap()

def get_begin_images(folder):
    global future_begin
    camera.cap_mutex.acquire()
    camera.cap_mutex.release()
    sucessos = 0
    log.log('future begin : '+ str(future_begin))
    log.log('last picutre : '+ str(time.time() - last_picture))
    if not (future_begin and time.time() - last_picture < 620):
        return snap(True) 
    else:
        future_begin = False
        return 4


class Transaction(Thread):

    def __init__ (self,supply,evento,user,qr):
        global last_unique_event
        Thread.__init__(self)
        self.id = evento
        self.user = user
        self.qr = qr
        self.status = 0
        self.begin_images = 0
        self.predicted_value = 0
        self.signed = not user is None
        self.supply = supply
        self.timeout = 3600 if supply else 120
        self.unique = last_unique_event
        self.after_begin = False
        last_unique_event += 1

    def end(self):
        global current_event
        if self.unique == current_event.unique:
            log.close()
            current_event = None
            if self.status != 13:
                Snap().start()

    def record_transaction(self,folder):
        self.status = 1
        times = [];
        
        to = Timeout(get_begin_images,20,-1,folder)
        sucessos = to.run()

        if sucessos ==  4:
            sucessos = move_images('last',folder,'last','begin')

        if not self.supply and sucessos != 4:
            self.status = 13
            self.begin_images = sucessos
            return times

        self.after_begin = True

        times.append(time.time()) #0
        times.append(time.time())#1
        self.status = 5

        events.add('folders',folder)
        events.add('events',self.id)

        door.open()
        if door.wait_door('CLOSED',10) == 'timeout':
            door.close()
            door_still_close = True
            for i in range(50):
                if door.sensor_door('CLOSED') == 'OPEN':
                    door_still_close = False
                    break
                time.sleep(0.1)
            if door_still_close:
                self.status = 6

        door.close()
        if self.status == 6 and door.sensor_door('') == 'CLOSED':
            camera.cap_mutex.acquire()
            camera.capture_images('end',folder)
            camera.cap_mutex.release()
            return times
        self.status = 7

        aux = None
        if self.supply:
            aux = camera.SupplyImages()
            aux.start()

        if door.wait_door('OPEN',self.timeout) == 'timeout' and not self.supply:
            self.id = send_ticket(tickets.porta_nao_fechou(self.id),self.id,self.qr,folder,self.user)
            pendencies.update(folder,{'event':self.id})
        self.status = 10

        if self.supply:
            aux.stop()

        times.append(time.time())#2
        camera.cap_mutex.acquire()
        sucessos = camera.capture_images('end',folder)
        camera.cap_mutex.release()
        if sucessos == 4:
            pendencies.update(folder,{'end':True})
        elif not self.supply:
            self.id = send_ticket(tickets.erro_end(self.id,sucessos),self.id,self.qr,folder,self.user)
            pendencies.update(folder,{'event':self.id})

        return times;

    def run(self):
        global current_event
        mutex.acquire()

        try:
            self.status = 0
            log.log('=======NOVA TRANSACAO==========')
            self.status = 0
            log.log('Event Id: ' + str(self.id))
            
            folder = log.timestamp()
            os.mkdir(paths.transacoes+'/'+folder)
            log.open_log(folder)
            log.log('Nova transação:')
            pendencies.update(folder,{'event':self.id,'user':self.user,'qr':self.qr,'buy':self.supply,'images':False,'end':False})

            times = self.record_transaction(folder)
            mutex.release()

            if self.status != 10:
                if door.error:
                    send_alert('ERRO LENDO STATUS DA TRAVA, TRANSAÇÃO FALHOU',True)
                if self.status == 6 and not self.supply:
                    self.id = send_ticket(tickets.porta_nao_abriu(self.id),self.id,self.qr,folder,self.user)
                if self.status == 13 and not self.supply:
                    self.id = send_ticket(tickets.erro_begin(self.id,self.begin_images),self.id,self.qr,folder,self.user)
                pendencies.update(folder,None)
                self.end()
                return

            if self.supply:
                if self.id is None:
                    self.id = signed_supply(folder,self.user,self.qr,False)
                self.status = 20
                upload_aws(folder,self.id,None)

            else:
                times.append(time.time())#3
                json_count = []
                pos = []
                buy_id = 0
                buy_data = None
                if config['PREDICT_LOCAL']:
                    #json_count, pos = predict.predict_local(paths.transacoes+'/'+self.id+'/');
                    times.append(time.time())#4
                    if(self.signed):
                        self.id,buy_id,buy_data = buy_signed(folder,self.user,self.qr,False)
                    else:
                        buy_id,buy_data = buy(folder,self.id,json_count,pos);
                    self.status = 11
                else:
                    self.status = 12
                    times.append(time.time())#4
                    if(self.id is None):
                        self.id,buy_id,buy_data = buy_signed(folder,self.user,self.qr,False)
                    else:
                        buy_id,buy_data = buy(folder,self.id,json_count,pos);

                log.log('event_id : %s, buy_id : %s' % (str(self.id),str(buy_id)))
                times.append(time.time())#5
                if not upload_aws(folder,self.id,buy_data):
                    self.id = send_ticket(tickets.erro_upload(self.id),self.id,self.qr,folder,self.user)
                self.status = 14
                times.append(time.time())#6

                register_times(buy_id,times);

            self.end()
        except Exception as e: 
            self.status = 13
            log.log('Erro geral: ' + str(e))
            self.end()
            mutex.release()
